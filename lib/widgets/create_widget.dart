import 'package:flutter/material.dart';

Widget onCreateCard({
  required BuildContext context,
  Function? actionClose,
  Function? actionGo,
  required String title,
  required String address,
  List<dynamic>? scheduledays,
  required String banner,
}) {
  if (banner != 'null') {
  }

  return Container(
    decoration:const  BoxDecoration(
      color: Colors.blueAccent,
      borderRadius:  BorderRadius.all(
        Radius.circular(20.0),
      ),
    ),
    child: Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: const EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 6.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: Text(
                  title,
                  maxLines: 2,
                  style: const TextStyle(
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.bold,
                    fontSize: 25.0,
                    // color: CustomTheme.of(context).darkText,
                  ),
                ),
              ),
            ),
            if (actionClose != null)
              IconButton(
                onPressed: () => actionClose(),
                icon:const  Icon(
                  Icons.close,
                  color: Colors.grey,
                  size: 25.0,
                ),
              ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      address,
                      style: const TextStyle(
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w500,
                        fontSize: 16.0,
                        //color: CustomTheme.of(context).darkText,
                      ),
                    ),
                  ),
                  
                ],
              ),
              
              if (scheduledays != null && scheduledays.isNotEmpty)
               const Text(
                  'Horarios',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                   // color: CustomTheme.of(context).darkText,
                  ),
                ),
              if (scheduledays != null && scheduledays.isNotEmpty)
                _Dropdown(list: scheduledays),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: 100.0,
                    child: ElevatedButton(onPressed: actionGo != null ? () => actionGo() : null, child: const Text('accion'))
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

class _Dropdown extends StatefulWidget {
  const _Dropdown({Key? key, required this.list}) : super(key: key);

  final List<dynamic> list;

  @override
  State<_Dropdown> createState() => _DropdownState();
}

class _DropdownState extends State<_Dropdown> {
  int count = 0;

  int actualValue = 0;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<dynamic>(
      value: actualValue,
      isExpanded: false,
      onChanged: (value) {
        actualValue = value;
      },
      underline: const SizedBox(),
      icon: const Icon(Icons.keyboard_arrow_down_rounded),
      items: widget.list.map<DropdownMenuItem<dynamic>>(
        (value) {
          return DropdownMenuItem<dynamic>(
            value: count++,
            child: Text(
              value['day'],
              style: TextStyle(
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w500,
                fontSize: MediaQuery.of(context).size.width * 0.04,
                //color: CustomTheme.of(context).darkText,
              ),
              textScaleFactor: 1,
            ),
          );
        },
      ).toList(),
    );
  }
}