import 'package:flutter/material.dart';


class DropDownListInput extends StatefulWidget {
  final List<String> items;
  final ValueChanged onChange;//void Function(String?)? onChange;
  final FocusNode? focus;
  final String? errorText;
  final String? valueS;
  final String? hint;
  final double? heigthList;

  const DropDownListInput({
    Key? key,
    required this.items,
    required this.onChange,
    this.errorText,
    this.focus,
    this.valueS,
    this.hint,
    this.heigthList,
  }) : super(key: key);

  @override
  State<DropDownListInput> createState() => _DropDownListInputState();
}

class _DropDownListInputState extends State<DropDownListInput> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                height: widget.heigthList,
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:const  BorderRadius.all(
                    Radius.circular(30),
                  ),
                  border: Border.all(
                    color: widget.errorText != null
                        ? Colors.red
                        : Colors.lightBlueAccent,
                    width: 2.0,
                  ),
                ),
                child: DropdownButton<String>(
                  hint: Text( widget.hint??'Ingrese un valor') ,
                  isExpanded: true,
                  focusNode: widget.focus,
                  value: widget.valueS,
                  underline: const SizedBox(),
                  style:  const TextStyle(color: Colors.black54,fontWeight: FontWeight.w500),
                  onChanged: (value) {
                    widget.onChange(value);
                  },
                  items: widget.items
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                            
                ),
              ),
            ),
          ],
        ),
        if (widget.errorText != null)
          Container(
            padding: const EdgeInsets.only(left: 15.0, top: 6.0, right: 10.0),
            child: Text(
              widget.errorText ?? '',
              style: const TextStyle(color: Colors.black54,fontWeight: FontWeight.w500),
            ),
          ),
        if (widget.errorText == null)
          const SizedBox(
            height: 10.0,
          ),
      ],
    );
  }
}