

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Widget commonInput({
  required BuildContext context,
  required Function actionOnChanged,
  required String labelText,
  required Key key,
  VoidCallback? onIconButton,
  bool? showIconButton,
  bool? enableInteractiveSelection,
  bool? alignLabelWithHint,
  TextInputType? keyboardType,
  String? errorText,
  Function? actionEditingComplete,
  Function? actionOnTap,
  Icon? icon,
  TextEditingController? controller,
  FocusNode? focus,
  bool readOnly = false,
  List<TextInputFormatter>? inputFormatters,
  TextAlign? textAlign,
  int? maxLines,
}) {
  InputDecoration textInputDecoration = InputDecoration(
    fillColor: enableInteractiveSelection == false
        ? Colors.white 
        : Colors.white,
    filled: true,
    hintText: 'Ingrese un valor',
    labelStyle: const TextStyle(color: Colors.black54, fontWeight: FontWeight.w500),
    contentPadding:
        const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
    border: const OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(30),
      ),
    ),
    enabledBorder: const OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.lightBlue,
        width: 1.0,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(30),
      ),
    ),
    focusedBorder: const OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.lightBlue,
        width: 2.0,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(30),
      ),
    ),
    suffixIcon: GestureDetector(
      key: key,
      onTapUp: (value) {
        onIconButton!();
      },
      child: Icon(
        (showIconButton == true) ? Icons.close : null,
        size: 25,
        color: Colors.blueAccent,
      ),
    ),
  );

  return TextField(
    enabled: enableInteractiveSelection ?? true,
    enableInteractiveSelection: enableInteractiveSelection ?? true,
    key: key,
    readOnly: readOnly,
    controller: controller,
    focusNode: focus,
    onChanged: (data) => actionOnChanged(data),
    onTap: actionOnTap != null ? () => actionOnTap() : null,
    keyboardType: keyboardType ?? TextInputType.text,
    decoration: textInputDecoration.copyWith(
      alignLabelWithHint: alignLabelWithHint,

      hintText: '',
      labelText: labelText,
      helperText: '',
      errorText: errorText ?? '',
      focusedErrorBorder: errorText != null
          ? null
          : const OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.blueAccent,
                width: 3.0,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
      errorBorder: errorText != null
          ? null
          : const OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.lightBlueAccent,
                width: 2.0,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
      errorStyle: errorText != null ? null : const TextStyle(fontSize: 0),
      suffixIcon: icon,
    ),
    style: const TextStyle(color: Colors.black),
    //onEditingComplete: () => actionEditingComplete!(),
    onEditingComplete:
        actionEditingComplete != null ? () => actionEditingComplete() : null,
    inputFormatters: inputFormatters ?? [LengthLimitingTextInputFormatter(15)],
    textAlign: textAlign ?? TextAlign.start,
    maxLines: maxLines ?? 1,
  );
}