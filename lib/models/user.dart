

class User {

  String? id;
  String? name;
  int? votes;
  String? firstName;
  String? otherNames;
  String? firstLastName;
  String? secondLastName;
  String? country;
  String? typeUserId;
  String? userId;
  String? email;
  String? area;
  String? startDate;
  //añadir fecha de ingreso, area, estado, fecha y hora de registro
  User({
    this.id,
    this.name,
    this.votes,
    this.firstName,
    this.otherNames,
    this.firstLastName,
    this.secondLastName,
    this.country,
    this.typeUserId,
    this.userId,
    this.email,
    this.area,
    this.startDate
  //añadir fecha de ingreso, area, estado, fecha y hora de registro

  });

  factory User.fromMap( Map<String, dynamic> obj ) 
    => User(
      id   : obj.containsKey('id') ? obj['id'] : 'no-id',
      name : obj.containsKey('name') ? obj['name'] : 'no-name',
      votes: obj.containsKey('votes') ? obj['votes'] : 'no-votes',
      firstName: obj.containsKey('firstName') ? obj['firstName'] : 'no-firstName',
      otherNames : obj.containsKey('otherNames') ? obj['otherNames'] : 'no-otherNames',
      firstLastName: obj.containsKey('firstLastName') ? obj['firstLastName'] : 'no-firstLastName',
      secondLastName: obj.containsKey('secondLastName') ? obj['secondLastName'] : 'no-secondLastName' ,
      country   : obj.containsKey('country') ? obj['country'] : 'no-country',
      typeUserId : obj.containsKey('typeUserId') ? obj['typeUserId'] : 'no-typeUserId',
      userId: obj.containsKey('userId') ? obj['userId'] : 'no-userId',
      email   : obj.containsKey('email') ? obj['email'] : 'no-email',
      area: obj.containsKey('jobArea') ? obj['jobArea'] : 'no-email',
      startDate: obj.containsKey('entryDate') ? obj['entryDate'] : 'no-entryDate',
    );
  


}