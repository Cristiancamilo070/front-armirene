import 'package:armirene_front/pages/update_user_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pie_chart/pie_chart.dart';

import '../models/user.dart';
import '../services/socket_service.dart';
import 'create_user_page.dart';


class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  List<User>? users = [];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    
    final socketService = Provider.of<SocketService>(context, listen: false);

    socketService.socket.on('active-users', _handleActiveUsers );
    super.initState();
  }

  _handleActiveUsers( dynamic payload ) {

  users = (payload as List)
        .map( (user) => User.fromMap(user) )
        .toList();

    setState(() {});
  }

  @override
  void dispose() {
    final socketService = Provider.of<SocketService>(context, listen: false);
    socketService.socket.off('active-users');
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    final socketService = Provider.of<SocketService>(context);

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text('Users', style: TextStyle( color: Colors.black87 ) ),
        backgroundColor: Colors.white,
        elevation: 1,
        actions: <Widget>[
          Container(
            margin: const EdgeInsets.only( right: 10 ),
            child: ( socketService.serverStatus == ServerStatus.online )
              ? Icon( Icons.check_circle, color: Colors.blue[300] )
              : const Icon( Icons.offline_bolt, color: Colors.red ),
          )
        ],
      ),
      body: Stack(
        children: <Widget>[

          SingleChildScrollView(child: Container(
            height:250 ,
             child: _showGraph())),

          Column(
            children: [
              SizedBox(height: 250,),
              Expanded(
                child: ListView.builder(
                  itemCount: users?.length,
                  itemBuilder: ( context, i ) => _userTile( users![i] )
                ),
              ),
            ],
          )

        ],
      ),
      floatingActionButton: FloatingActionButton(
        elevation: 1,
        backgroundColor: Colors.lightBlueAccent,
        onPressed: () {
           Navigator.push(
                context,
                PageRouteBuilder(
                  opaque: false,
                  transitionDuration: const Duration(milliseconds: 500),
                  transitionsBuilder:
                      (context, animation, secondaryAnimation, child) {
                    animation = CurvedAnimation(
                        parent: animation, curve: Curves.easeIn);
                    return FadeTransition(
                      //alignment: Alignment.center,
                      //scale: animation,
                      opacity: animation,
                      child: child,
                    );
                  },
                  pageBuilder: (_, __, ___) => CreateUserPage(
                    actionClose: () {
                      Navigator.pop(context);
                    },
                    users: users!,
                  ),
                ));
        },
        child: const Icon( Icons.add )
      ),
   );
  }

  Widget _userTile( User user ) {

    final socketService = Provider.of<SocketService>(context, listen: false);
    var parts = user.email!.split('@');
    var prefix = parts[0].trim(); 
    
    return Dismissible(
      key: Key(user.id!),
      direction: DismissDirection.startToEnd,
      onDismissed: ( _ ) => socketService.emit('delete-user', { 'id': user.id }),
      background: Container(
        padding: const EdgeInsets.only( left: 8.0 ),
        color: Colors.red,
        child: const Align(
          alignment: Alignment.centerLeft,
          child: Text('Delete User', style: TextStyle( color: Colors.white) ),
        )
      ),
      child: ListTile(
        leading: CircleAvatar(
          backgroundColor: Colors.blue[100],
          child: Text( user.firstLastName!.substring(0,2) ),
        ),
        title: Text( '${user.firstName!} ${user.firstLastName!}'),
        trailing: Text('${prefix}'),
        onTap: () {
        //aca
        socketService.socket.emit('times-it-went-user', { 'id': user.id }) ;
        },
        onLongPress: (){
          Navigator.push(
                context,
                PageRouteBuilder(
                  opaque: false,
                  transitionDuration: const Duration(milliseconds: 500),
                  transitionsBuilder:
                      (context, animation, secondaryAnimation, child) {
                    animation = CurvedAnimation(
                        parent: animation, curve: Curves.easeIn);
                    return FadeTransition(
                      //alignment: Alignment.center,
                      //scale: animation,
                      opacity: animation,
                      child: child,
                    );
                  },
                  pageBuilder: (_, __, ___) => UpdateUserPage(
                    actionClose: () {
                      Navigator.pop(context);
                    },
                    users: users!,
                    user:user,
                  ),
                ));

        },
      ),
    );
  }

  // Mostrar gráfica
  Widget _showGraph() {

    Map<String, double> dataMap =  Map();
    // dataMap.putIfAbsent('Flutter', () => 5);
    users?.forEach((user) {
      var parts = user.email!.split('@');
      var prefix = parts[0].trim(); 
      dataMap.putIfAbsent( prefix, () => user.votes!.toDouble() );
    });

    final List<Color> colorList = [
      const Color.fromARGB(255, 204, 48, 48),
      const Color.fromARGB(255, 144, 249, 188),
      const Color.fromARGB(255, 63, 72, 234),
      Colors.pink[200]!,
      Colors.yellow[50]!,
      Colors.yellow[200]!,
      const Color.fromARGB(255, 53, 212, 98),
      const Color.fromARGB(255, 228, 38, 222),
      const Color.fromARGB(255, 255, 172, 157),
    ];

    return SingleChildScrollView(
      child: Column(
        children: [
          const Text(
              'Asistencia',
              maxLines: 2,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25.0,),),
          Container(
            padding:const EdgeInsets.only(top: 10),
            width: double.infinity,
            //height: 200,
            child: (dataMap.isNotEmpty)?
             PieChart(
            dataMap: dataMap,
            animationDuration: const Duration(milliseconds: 800),
            chartLegendSpacing: 32,
            chartRadius: MediaQuery.of(context).size.width / 3.2,
            colorList: colorList,
            initialAngleInDegree: 0,
            chartType: ChartType.ring,
            ringStrokeWidth: 32,
            centerText: "Users",
            legendOptions: const LegendOptions(
              showLegendsInRow: false,
              legendPosition: LegendPosition.right,
              showLegends: true,
              legendShape: BoxShape.circle,
              legendTextStyle: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            chartValuesOptions:const  ChartValuesOptions(
              showChartValueBackground: true,
              showChartValues: true,
              showChartValuesInPercentage: false,
              showChartValuesOutside: true,
              decimalPlaces: 1,
            ),
            // gradientList: ---To add gradient colors---
            // emptyColorGradient: ---Empty Color gradient---
          ):SizedBox(
              height: MediaQuery.of(context).size.height / 1.3,
              child:const  Center(
                child: CircularProgressIndicator(),
                  ),
              )
          ),
        ],
      ),
    );
  }

}

