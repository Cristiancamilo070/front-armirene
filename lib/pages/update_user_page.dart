import 'dart:math';

import 'package:armirene_front/models/user.dart';
import 'package:armirene_front/services/socket_service.dart';
import 'package:armirene_front/widgets/dropdown_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../widgets/common_input.dart';

class UpdateUserPage extends StatefulWidget {
  const UpdateUserPage(
      {Key? key,
      this.actionClose,
      this.actionSend,
      required this.users,required this.user, 
      })
      : super(key: key);

  final VoidCallback? actionClose;
  final VoidCallback? actionSend;
  final List<User> users; 
  final User user;  
  @override
  State<UpdateUserPage> createState() => _UpdateUserPageState();
}

class _UpdateUserPageState extends State<UpdateUserPage> {
  String? idType ;  
  var itemsIdType = [   
    'CC',
    'Pasaporte',
    'CE',
    'Permiso especial',
  ];
  String? country ;  
  var itemsCountry = [   
    'Estados Unidos',
    'Colombia',
  ];
  String? area ;  
  var itemsArea = [   
    'Administración',
    'Financiera',
    'Compras',
    'Infraestructura',
    'Operación',
    'Recursos humanos',
    'Desarrollo',
  ];

  
  @override
  Widget build(BuildContext context) {
    TextEditingController firstNameController = TextEditingController();
    TextEditingController otherNamesController = TextEditingController();
    TextEditingController firstLastNameController = TextEditingController();
    TextEditingController secondLastNameController = TextEditingController();
    TextEditingController idNumberController = TextEditingController();
    TextEditingController startDateController = TextEditingController();
    
    firstNameController.text=widget.user.firstName! ;
    otherNamesController.text=widget.user.otherNames! ;
    firstLastNameController.text=widget.user.firstLastName! ;
    secondLastNameController.text=widget.user.secondLastName! ;
    idNumberController.text=widget.user.userId! ;
    startDateController.text=widget.user.startDate! ;
    

    DateTime now =  DateTime.now();
    DateTime date = DateTime(now.year, now.month, now.day);
    String nowdate=date.toString();

    String dominio=(country=='Colombia')?'@armirene.com.co':'@armirene.com.us';

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.black.withOpacity(0.6),
      body: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Container(
          //height: 800,
          color: Colors.transparent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {},
                child: Padding(
                  padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                  child: SingleChildScrollView(
                    child: Container(
                      padding:const EdgeInsets.only(left: 10.0, right: 10.0),
                      decoration:const  BoxDecoration(
                        color: Colors.white,
                        borderRadius:  BorderRadius.all(
                          Radius.circular(20.0),
                        ),
                      ),
                      child: Column(
                        children: [
                          TitleWidget(actionClose: widget.actionClose),
                          const SizedBox(height: 20,),
                          countryWidget(),
                          const SizedBox(height: 10,),
                          areaWidget(),
                          const SizedBox(height: 10,),
                          typeIdWidget(),
                          const SizedBox(height: 10,),
                          _IdNumberInput(inputController: idNumberController),
                           _FirstNameInput(inputController: firstNameController),
                          const SizedBox(height: 10,),
                           _OtherNamesInput(inputController: otherNamesController),
                          const SizedBox(height: 10,),
                           _FirstLastNamesInput(inputController: firstLastNameController),
                          const SizedBox(height: 10,),
                           _SecondLastNamesInput(inputController: secondLastNameController),
                          const SizedBox(height: 10,),
                          _SelectstartDate(startDateController: startDateController,),
                          //const _SelectEndDate(),

                          ElevatedButton(onPressed: () async{
                            bool? sendOrNot;
                            bool? existingEmail;
                            var rng = Random();
                            int randomNumber = rng.nextInt(100); // from 0 upto 99 included


                            for (var i = 0; i < widget.users.length; i++) {//&&widget.users[i].typeUserId==idType
                              if ( widget.users[i].userId==idNumberController.text) {
                                sendOrNot=false;  
                                break;
                                 }else{
                                  sendOrNot=true;  
                                 } }
                            print('print send or not $sendOrNot');

                            if (sendOrNot==true) {
                            String email=firstNameController.text+firstLastNameController.text+dominio;
                            for (var i = 0; i < widget.users.length; i++) {
                              if ( widget.users[i].email==email.toLowerCase() && widget.users[i].typeUserId==widget.user.typeUserId) {
                                setState(() {
                                existingEmail==true;//ya existe  
                                email=firstNameController.text+firstLastNameController.text+randomNumber.toString() +dominio;
                                });
                                break;
                                 }else{
                                  existingEmail=false;     
                                 } 
                            }
                            addUserToList(firstNameController.text, otherNamesController.text, firstLastNameController.text, secondLastNameController.text, widget.user.country!, widget.user.typeUserId!, idNumberController.text,startDateController.text, nowdate, widget.user.area!,email.toLowerCase());
                            }else{
                            const snackBar = SnackBar(
                              content: Text('Este documento ya existe'),
                            );
                            ScaffoldMessenger.of(context).showSnackBar(snackBar);
                          
                            } 
                          }, child: const  Text(' ACTUALIZAR '))
                        ],
                      ),
                    ),
                  )
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget typeIdWidget(){
    return DropDownListInput(
      items: itemsIdType,
      valueS: (widget.user.typeUserId!.isEmpty)?idType: widget.user.typeUserId,
      hint: 'Ingrese tipo de documento',
      onChange:  (newValue) {
        setState(() {
          idType = newValue;

        });
      },
      );
  }

    Widget areaWidget(){
    return DropDownListInput(
      items: itemsArea,
      valueS: widget.user.area,
      hint: 'Ingrese el área laboral',
      onChange:  (newValue) {
        setState(() {
          area = newValue;

        });
      },
      );
  }

  Widget countryWidget(){
    return DropDownListInput(
      items: itemsCountry,
      valueS: (widget.user.country!.isEmpty)?country: widget.user.country,
      hint: 'Ingrese pais de empleo',
      onChange:  (newValue) {
        setState(() {
          country = newValue;

        });
      },
      );
  }

  void addUserToList( String firstName,String otherNames, String firstLastName, String secondLastName,String country,String typeUserId, String userId, String entryDate, String nowDate,String area,String email) {
    if ( firstName.length > 1 ) {
      final socketService = Provider.of<SocketService>(context, listen: false);
      socketService.emit('update-user', { 'firstName': firstName,'otherNames': otherNames,'firstLastName': firstLastName,'secondLastName': secondLastName, 'country': country, 'typeUserId': typeUserId,'userId':userId, 'entryDate':entryDate, 'nowDate':nowDate, 'jobArea':area,'email':email, 'id': widget.user.id});
    }
    Navigator.pop(context);
  }
}

class TitleWidget extends StatelessWidget {
  const TitleWidget({
    Key? key,
    required this.actionClose,
  }) : super(key: key);

  final VoidCallback? actionClose;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          padding: const EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 6.0),
          child: SizedBox(
            width: MediaQuery.of(context).size.width * 0.5,
            child: const Text(
              'Actualizar usuario',
              maxLines: 2,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25.0,),),),),
        if (actionClose != null)
          IconButton(
            onPressed: () => Navigator.pop(context),
            icon:const  Icon(
              Icons.close,
              color: Colors.grey,
              size: 25.0,
            ),
          ),
    ],
                      );
  }
}

// ignore: must_be_immutable
class _FirstNameInput extends StatelessWidget {
  _FirstNameInput({Key? key, required this.inputController}) : super(key: key);
  TextEditingController inputController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return commonInput(
              onIconButton: () {
                FocusManager.instance.primaryFocus?.unfocus();
                inputController.clear();
              },
              maxLines: 1,
              key: const Key('input_feedback_key'),
              controller: inputController,
              context: context,
              icon: const Icon(Icons.person_add_alt),
              actionOnChanged: (data) {
                // context
                //     .read<FeedbackCubit>()
                //     .changeCommentText(feedbackController.text);
              },
              alignLabelWithHint: true,
              labelText: 'Ingrese su primer nombre',
              inputFormatters: [
                LengthLimitingTextInputFormatter(20),
              ],
            );
  }
}

// ignore: must_be_immutable
class _OtherNamesInput extends StatelessWidget {
  _OtherNamesInput({Key? key,required this.inputController}) : super(key: key);
  TextEditingController inputController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return commonInput(
              onIconButton: () {
                FocusManager.instance.primaryFocus?.unfocus();
                inputController.clear();
              },
              maxLines: 1,
              key: const Key('input_feedback_key'),
              controller: inputController,
              context: context,
              icon: const Icon(Icons.person_add_alt),
              actionOnChanged: (data) {
                // context
                //     .read<FeedbackCubit>()
                //     .changeCommentText(feedbackController.text);
              },
              alignLabelWithHint: true,
              labelText: 'Ingrese su segundo nombre',
              inputFormatters: [
                LengthLimitingTextInputFormatter(50),
              ],
            );
  }
}

// ignore: must_be_immutable
class _FirstLastNamesInput extends StatelessWidget {
  _FirstLastNamesInput({Key? key,required this.inputController}) : super(key: key);
  TextEditingController inputController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return commonInput(
              onIconButton: () {
                FocusManager.instance.primaryFocus?.unfocus();
                inputController.clear();
              },
              maxLines: 1,
              key: const Key('input_feedback_key'),
              controller: inputController,
              context: context,
              icon: const Icon(Icons.person_add_alt),
              actionOnChanged: (data) {
                // context
                //     .read<FeedbackCubit>()
                //     .changeCommentText(feedbackController.text);
              },
              alignLabelWithHint: true,
              labelText: 'Ingrese su primer apellido',
              inputFormatters: [
                LengthLimitingTextInputFormatter(20),
              ],
            );
  }
}

// ignore: must_be_immutable
class _SecondLastNamesInput extends StatelessWidget {
   _SecondLastNamesInput({Key? key,required this.inputController}) : super(key: key);
  TextEditingController inputController = TextEditingController();
  @override
  Widget build(BuildContext context) {
      
    return commonInput(
              onIconButton: () {
                FocusManager.instance.primaryFocus?.unfocus();
                inputController.clear();
              },
              maxLines: 1,
              key: const Key('input_feedback_key'),
              controller: inputController,
              context: context,
              icon: const Icon(Icons.person_add_alt),
              actionOnChanged: (data) {
                // context
                //     .read<FeedbackCubit>()
                //     .changeCommentText(feedbackController.text);
              },
              alignLabelWithHint: true,
              labelText: 'Ingrese su segundo apellido',
              inputFormatters: [
                LengthLimitingTextInputFormatter(20),
              ],
            );
  }
}

// ignore: must_be_immutable
class _IdNumberInput extends StatelessWidget {
   _IdNumberInput({Key? key,required this.inputController}) : super(key: key);
  TextEditingController inputController = TextEditingController();
  @override
  Widget build(BuildContext context) {
      
    return commonInput(
              onIconButton: () {
                FocusManager.instance.primaryFocus?.unfocus();
                inputController.clear();
              },
              maxLines: 1,
              key: const Key('input_feedback_key'),
              controller: inputController,
              context: context,
              icon: const Icon(Icons.numbers),
              actionOnChanged: (data) {
                // context
                //     .read<FeedbackCubit>()
                //     .changeCommentText(feedbackController.text);
              },
              alignLabelWithHint: true,
              labelText: 'Ingrese el número de documento',
              inputFormatters: [
                LengthLimitingTextInputFormatter(20),
              ],
            );
  }
}

// ignore: must_be_immutable
class _SelectstartDate extends StatefulWidget {
   _SelectstartDate({Key? key,required this.startDateController}) : super(key: key);
  TextEditingController startDateController = TextEditingController();
  @override
  State<_SelectstartDate> createState() => _SelectstartDateState();
}

class _SelectstartDateState extends State<_SelectstartDate> {
  final DateFormat dateFormatter = DateFormat('yyyy-MM-dd');

  @override
  Widget build(BuildContext context) {
    return SizedBox(
          height: 60,
          child: commonInput(
            key: const Key('date_init_input_key'),
            readOnly: true,
            controller: widget.startDateController,
            context: context,
            actionOnChanged: (data) {},
            actionOnTap: () async {
              _appDatePicker(context, (date) {
                widget.startDateController.text = dateFormatter.format(date);
              });
            },
            icon: const Icon(Icons.date_range),
            labelText: 'Fecha de ingreso',
            errorText: null
            // focus: widget.ownFocus,
            // actionEditingComplete: () => widget.nextFocus.requestFocus(),
          ),
        );
  }
}

void _appDatePicker(BuildContext context, Function onConfirm) {
  final DateTime now = DateTime.now().toLocal();

  DatePicker.showDatePicker(
    context,
    showTitleActions: true,
    minTime: DateTime.utc(now.year, now.month-1,now.day-30 ),
    maxTime: DateTime(now.year, now.month, now.day),
    theme: const DatePickerTheme(
      cancelStyle:
          TextStyle(color: Colors.white, fontSize: 18),
      headerColor: Colors.lightBlueAccent,
      backgroundColor: Colors.white,
      itemStyle: TextStyle(
        color: Colors.black54,
        fontWeight: FontWeight.bold,
        fontSize: 18,
      ),
      doneStyle: TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.bold,
        fontSize: 16,
      ),
    ),
    onChanged: (date) {},
    onConfirm: (date) => onConfirm(date),
    currentTime: now,
    locale: LocaleType.es,
  );
}
