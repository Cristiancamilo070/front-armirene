import 'package:flutter/material.dart';

import 'package:socket_io_client/socket_io_client.dart' ;

enum ServerStatus{
  online,
  offline,
  connecting
}


class SocketService with ChangeNotifier {

  ServerStatus _serverStatus = ServerStatus.connecting;
  late Socket _socket;

  ServerStatus get serverStatus => _serverStatus;
  
  Socket get socket => _socket;
  Function get emit => _socket.emit;


  SocketService(){
    _initConfig();
  }

  void _initConfig() {
    
    // Dart client
    _socket = io('https://armirene-back.herokuapp.com/', { // http://192.168.10.6:3000/
      'transports': ['websocket'],
      'autoConnect': true
    });

    _socket.on('connect', (_) {
      _serverStatus = ServerStatus.online;
      notifyListeners();
    });

    _socket.on('disconnect', (_) {
      _serverStatus = ServerStatus.offline;
      notifyListeners();
    });

  }

}